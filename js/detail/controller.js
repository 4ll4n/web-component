import {create} from './component.js';

export class NoteDetailController {
  constructor(root, router, repository, noteId) {
    this.router = router;
    this.repository = repository;

    create({
      root,
      saveListener: this.save.bind(this),
      deleteListener: this.delete.bind(this),
      note: repository.get(noteId)
    });
  }

  save({ detail: note }) {
    this.repository.update(note);
    this.router.navigate(this.router.generate('notes.list'));
  }

  delete({ detail: noteId }) {
    this.repository.delete(noteId);
    this.router.navigate(this.router.generate('notes.list'));
  }
};
