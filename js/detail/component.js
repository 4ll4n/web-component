import {LitElement, html} from 'lit-element';
import {render} from 'lit-html';
import '@material/mwc-textfield';
import '@material/mwc-textarea';
import '@material/mwc-button';

const ELEMENT = 'note-detail',
      EVENT_SAVE = 'note.save',
      EVENT_DELETE = 'note.delete';

const create = ({ root, saveListener, deleteListener, note }) => {
  render(html`<note-detail></note-detail>`, root);

  const detail = document.querySelector(ELEMENT);

  detail.addEventListener(EVENT_SAVE, saveListener);
  detail.addEventListener(EVENT_DELETE, deleteListener);

  detail.note = note;
};

class NoteDetailComponent extends LitElement {
  static get properties() {
    return {
      note: { type: Object }
    };
  }

  constructor() {
    super();
  }

  render() {
    return html`
      ${this.note
        ? html`
          <mwc-textfield id="note-title" label="Title" outlined .value="${this.note.title || ''}"></mwc-textfield>
          <mwc-textarea fullwidth label="Content" .value="${this.note.content || ''}"></mwc-textarea>
          <mwc-button outlined label="save" @click="${this.save}"></mwc-button>
          <mwc-button outlined label="delete" @click="${this.delete}"></mwc-button>`
        : html`loading...`
      }
    `;
  }

  save() {
    this.note.title = this.renderRoot.querySelector('mwc-textfield').value;
    this.note.content = this.renderRoot.querySelector('mwc-textarea').value;
    let event = new CustomEvent(EVENT_SAVE, { detail: this.note });
    this.dispatchEvent(event);
  }

  delete() {
    let event = new CustomEvent(EVENT_DELETE, { detail: this.note.id });
    this.dispatchEvent(event);
  }
}

customElements.define(ELEMENT, NoteDetailComponent);

export { create };
