const create = () => {
    return new NotesRepository();
}

class NotesRepository {
    constructor() {
        this.id = 1;
        this.notes = [];
    }

    all() {
        return this.notes;
    }

    get(id) {
        return this.notes.find(note => note.id === id);
    }

    update(note) {
        Object.assign(this.get(note.id), note);
    }

    delete(id) {
        this.notes = this.notes.filter(note => note.id !== id);
    }

    create() {
        const note = { id: this.id++ };
        this.notes.push(note);
        return note;
    }
};

export { create };
