import * as MemoryRepository from './memory.js';

const create = () => {
    return new NotesRepository(MemoryRepository.create());
}

class NotesRepository {
    constructor(repository) {
        this.repository = repository;
        this.repository.id = Number.parseInt(localStorage.getItem('notes.id') || '1', 10);
        this.repository.notes = JSON.parse(localStorage.getItem('notes')) || [];
    }

    all() {
        return this.repository.all();
    }

    get(id) {
        return this.repository.get(id);
    }

    update(note) {
        this.repository.update(note);
        this._save();
    }

    delete(id) {
        this.repository.delete(id);
        this._save();
    }

    create() {
        const note = this.repository.create();
        this._save();
        return note;
    }

    _save() {
        localStorage.setItem('notes', JSON.stringify(this.repository.notes));
        localStorage.setItem('notes.id', JSON.stringify(this.repository.id));
    }
};

export { create };
