import * as NotificationService from '../notification/service.js';

const create = (repository) => {
    return new NotificationRepository(repository, NotificationService.create());
}

class NotificationRepository {
    constructor(repository, notification) {
        this.repository = repository;
        this.notification = notification;
    }

    all() {
        return this.repository.all();
    }

    get(id) {
        return this.repository.get(id);
    }

    update(note) {
        this.repository.update(note);
        this.notification.show(`✏️ Note ${note.id} updated`);
    }

    delete(id) {
        this.repository.delete(id);
        this.notification.show(`🗑️ Note ${id} deleted`);
    }

    create() {
        const note = this.repository.create();
        this.notification.show(`➕ Note ${note.id} created`);
        return note;
    }
};

export { create };
