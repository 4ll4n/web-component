const create = () => {
    const snackbar = document.querySelector('mwc-snackbar');
    return new NotificationService(snackbar);
}

class NotificationService {
    constructor(snackbar) {
        this.snackbar = snackbar;
    }

    show(message) {
        this.snackbar.labelText = message;
        this.snackbar.show();
    }
}

export { create };
