import '@material/mwc-top-app-bar-fixed';
import '@material/mwc-snackbar';

import { NoteListController } from './list/controller.js';
import { NoteDetailController } from './detail/controller.js';
import * as LocalRepository from './repository/localstorage.js';
import * as NotificationRepository from './repository/notification.js';

const repository = NotificationRepository.create(LocalRepository.create());

const content = document.querySelector('#content');

const router = new Navigo(null, true);

router.on({
  '/notes/:id': {
    as: 'notes.edit',
    uses: ({ id }) => new NoteDetailController(content, router, repository, Number.parseInt(id, 10))
  },

  '/notes': {
    as: 'notes.list',
    uses: () => new NoteListController(content, router, repository)
  },

  '/': {
    as: 'home',
    uses: () => router.navigate(router.generate('notes.list'))
  }
}).resolve();
