import { create } from './component.js';

export class NoteListController {
  constructor(root, router, repository) {
    this.router = router;
    this.repository = repository;

    create({
      root,
      selectListener: this.select.bind(this),
      addListener: this.add.bind(this),
      notes: repository.all()
    });
  }

  select({ detail: note }) {
    this.router.navigate(this.router.generate('notes.edit', note));
  }

  add() {
    const note = this.repository.create();
    this.router.navigate(this.router.generate('notes.edit', note));
  }
};
