import {LitElement, html} from 'lit-element';
import { render } from 'lit-html';
import '@material/mwc-list/mwc-list.js';
import '@material/mwc-list/mwc-list-item.js';
import '@material/mwc-fab';
import '@material/mwc-icon';

const ELEMENT = 'note-list',
      EVENT_SELECT = 'notes.select',
      EVENT_ADD = 'notes.add';

const create = ({root, selectListener, addListener, notes}) => {
  render(html`<note-list></note-list>`, root);
  const list = root.querySelector(ELEMENT);
  list.addEventListener(EVENT_SELECT, selectListener);
  list.addEventListener(EVENT_ADD, addListener);
  list.notes = notes;
};

class NoteListComponent extends LitElement {
  static get properties() {
    return {
      notes: { type: Array }
    };
  }

  constructor() {
    super();
  }

  select(note) {
    let event = new CustomEvent(EVENT_SELECT, { detail: note, bubbles: true });
    this.dispatchEvent(event);
  }

  add() {
    let event = new CustomEvent(EVENT_ADD, { bubbles: true });
    this.dispatchEvent(event);
  }

  render() {
    return html`
      <mwc-list id="list">
        ${this.notes.map(this.renderNote.bind(this))}
      </mwc-list>
      <mwc-fab icon="add" @click="${this.add}"></mwc-fab>
    `;
  }

  renderNote(note) {
    return html`
      <mwc-list-item twoline @click="${(e) => this.select(note)}">
        ${note.title}
        <span slot="secondary">${note.content}</span>
      </mwc-list-item>
    `;
  }
}

customElements.define(ELEMENT, NoteListComponent);

export { create };
